#include <SoftwareSerial.h>
#include <FastLED.h>
#include <Math.h>
#include <MemoryFree.h>
#include <elapsedMillis.h>
#include "LinkedList.h"

#define LED_1 2
#define LED_2 3
#define LED_3 4

#define NUM_LEDS 12
#define DOTSTAR_DATA_PIN 6
#define DOTSTAR_CLOCK_PIN 5

#define AT09_RX 8
#define AT09_TX 9


boolean debugEnabled = true;
boolean debugDuringExecution = false;
boolean debugDuringCommunication = true;

uint8_t colorStrength[] = {1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 13, 16, 19, 23, 27, 32, 38, 45, 54, 64, 76, 91, 108, 128, 152, 181, 215, 256 };



SoftwareSerial bleSerial(AT09_RX, AT09_TX);
elapsedMillis timeElapsed;
LinkedList<String*> commands = LinkedList<String*>();

uint8_t variable[26];

boolean errorState = false;


CRGB leds[NUM_LEDS];
uint8_t max_bright = 255;

int commandIndex = 0; // next command to execute
int delayMillis = 0;
boolean communicating = false;


void setup(){
	bleSerial.begin(9600);
	Serial.begin(115200);
	pinMode(LED_1, OUTPUT);
	pinMode(LED_2, OUTPUT);
	pinMode(LED_3, OUTPUT);

	FastLED.addLeds<APA102, DOTSTAR_DATA_PIN, DOTSTAR_CLOCK_PIN, BGR>(leds, NUM_LEDS);
	FastLED.setBrightness(max_bright);


	digitalWrite(LED_1, HIGH);
	delay(75);
	digitalWrite(LED_1, LOW);
	delay(75);
	digitalWrite(LED_1, HIGH);
	delay(75);
	digitalWrite(LED_1, LOW);
	delay(75);
	digitalWrite(LED_1, HIGH);
	delay(500);
	digitalWrite(LED_1, LOW);
	debug(F("freeMemory()="));
	debugln(freeMemory());
debugln("started1");
	resetVars();
	
	addCommand("h001100|2");
	addCommand("h220000|1");
	
	
	//addCommand("vx=0");
	//addCommand("h0|a");
	//addCommand("lloop");
	//addCommand("h008800|x");
	//addCommand("dc1");
	//addCommand("h000000|z");
	//addCommand("h001100|y");
	//addCommand("h002200|x");
	//addCommand("vz=y");
	//addCommand("vy=x");
	//addCommand("vx+1");
	//addCommand("?x<12|loop");
	//addCommand("vx=0");
	//addCommand("gloop");
	debugln("started");
}

void addCommand(String command) {
String *toAdd = new String(command);
commands.add(toAdd);
}


void loop() {
	bleMain();

	if (delayMillis > 0 ) {
		delayMillis --;
		delay(1);
	}
	else {
		dotStarMain();
	}
}

void dotStarMain() {
	if (commands.size() == 0 || errorState ) {
		delayMillis = 10000;
		return;
	}

	while (true) {
		if ( commandIndex == commands.size()) {
			commandIndex = 0;
		}

		String *command = commands.get(commandIndex);
		commandIndex ++;
		debug(F("executing: "));
		debug(*command);
		parseMessage(*command);
		if (command->startsWith(F("d"))) {
			break;
		}
	}
	FastLED.show();
}

void bleMain() {
	if (bleSerial.available()) {
		debugln(F("--- --- --- new message --- --- ---"));
		communicating = true;
		debugEnabled = debugDuringCommunication;
		boolean ok = readCommunicationHeader();
		if (ok) {
			boolean error = readEffectCommunication();
		}
		discardRemainingCommunication();
		communicating = false;
		debugEnabled = debugDuringExecution;
	}
}


boolean readCommunicationHeader() {
	timeElapsed = 0;
	String header = "";

	while (true) {
		if (bleSerial.available()) {
			timeElapsed = 0;
			char c = bleSerial.read();
			header.concat(c);
			if (header.length() == 3) {
				break;
			}
		}
		else {
			delayMicroseconds(1);
			if (timeElapsed > 250) {
				return false;
			}
		}
	}

	String LIGHT_EFFECT_HEADER = "*l{";
	return (LIGHT_EFFECT_HEADER == header);
}


boolean readEffectCommunication() {
	clearCommands();
	resetVars();
	errorState = false;
	timeElapsed = 0;
	long transmissionStart = millis();

	boolean finished = false;
	String command = "";
	while (!finished) {
		if (bleSerial.available()) {
			timeElapsed = 0;
			char c = bleSerial.read();
			if (c == '}') {
			break;
		}

		if (!validChar(c)) {
				debug(F("ERROR: invalid character received :'"));
				debug(c);
				debugln(F("'"));
				errorState = true;
				finished = true;
				break;
		}

		c = toLower(c);

		if (c == ';') {
			debug(F("Received: '"));
			debug(command);
			debug(F(";'    - "));
			finished = parseMessage(command);

			debug(F("   freeMemory()="));
			debugln(freeMemory());


			if (!finished) {
				String *toAdd = new String(command);
				commands.add(toAdd);
			}

			command = "";
		}
		else {
			command.concat(c);
		}
	}
 		else {
			delayMicroseconds(1);
			if (timeElapsed > 250) {
				debugln(F("timeout during read."));
				errorState = true;
				break;
			}
		}
	}


	debug(F("freeMemory()="));
	debugln(freeMemory());

	long transmissionEnd = millis();

	debugln(F(""));
	debug(F("Parsing took: "));
	debug(transmissionEnd - transmissionStart);
	debug(F(" millis.     "));
	if (errorState) {
		debugln(F("Error occurred during parsing, effect rejected."));
	}
	else {
		debugln(F("Parse success!"));
		commandIndex = 0;
		delayMillis = 0;
	}

	return errorState;
}

char toLower(char c) {
	if (c >= 'A' && c <= 'Z') {
		return 'a' + (c - 'A');
	}
	return c;
}

boolean validChar(char c) {
	if (validStringChar(c)) { return true; }
	if (c >= 'A' && c <= 'Z') { return true; }
	if (c == '{' || c == '}') { return true; }
	if (c == '+' || c == ' ') { return true; }
	if (c == '<' || c == '>') { return true; }
	if (c == '*' || c == ';') { return true; }
	if (c == '=' || c == '?') { return true; }
	if (c == '|' || c == '.') { return true; }
	return false;
}

boolean validStringChar(char c) {
	if (c >= 'a' && c <= 'z') { return true; }
	if (c >= '0' && c <= '9') { return true; }
	if (c == '-') { return true; }
	return false;
}

boolean discardRemainingCommunication() {
	timeElapsed = 0;
	boolean finished = false;

	String toDiscard = "";
	while (!finished) {
		if (bleSerial.available()) {
			timeElapsed = 0;
			char c = bleSerial.read();
			toDiscard.concat(c);
		}
		else {
			delayMicroseconds(1);
			if (timeElapsed > 250) {
				finished = true;
			}
		}
	}

	debug(F("Discarding: '"));
	debug(toDiscard);
	debugln(F("'."));
}



boolean parseMessage(String command) {
	if (command.length() == 0) {
		return true;
	}
	char type = command.charAt(0);
	String argument = command.substring(1);

	boolean finished = paresCommand(type, argument, command);
	return finished;
}

boolean paresCommand(char type, String argument, String command) {
	switch(type) {
		case 'p' : return parseColor(argument, command, true);
		case 'h' : return parseColor(argument, command, false);
		case 'd' : return parseDelay(argument, command);
		case 'v' : return parseVariable(argument, command);
		case 'l' : return parseLabel(argument, command);
		case 'g' : return parseGoto(argument, command);
		case '?' : return parseCompare(argument, command);
		default  :
			return parseError(F("Unknown command type"), String(type), F("type"), command);
	}
}



// -------------------- command processing methods --------------------



boolean parseColor(String argument, String command, boolean powerColor) {
	uint8_t pipeIndex = argument.indexOf("|");
	String colorPart = argument.substring(0, pipeIndex);
	String ledPart = argument.substring(pipeIndex +1);

	uint8_t r;
	uint8_t g;
	uint8_t b;
	
	if (colorPart.length() != 1 && colorPart.length() != 6) {
			return parseError(F("Invalid length of argument. Length must be 1 or 6."), String(colorPart), F("(color part)"), command);
	}
	if (colorPart.length() == 1) {
		if (colorPart == F("0")) {
			r = 0;
			g = 0;
			b = 0;
		}
		else {
			return parseError(F("Shorthand (color part) only allowed for value '0'."), String(colorPart), F("(color part)"), command);
		}
	}
	else {
		String rr = colorPart.substring(0,2);
		String gg = colorPart.substring(2,4);
		String bb = colorPart.substring(4,6);

		r = parseVarnumberhex(rr, F("<rr>"), command);
		g = parseVarnumberhex(gg, F("<gg>"), command);
		b = parseVarnumberhex(bb, F("<bb>"), command);

		if (powerColor) {
			if (r > 24) {
				return parseError(F("Value too high for strength multiplier"), String(r), F("<rr>"), command);
			}
			if (g > 24) {
				return parseError(F("Value too high for strength multiplier"), String(r), F("<gg>"), command);
			}
			if (g > 24) {
				return parseError(F("Value too high for strength multiplier"), String(r), F("<bb>"), command);
			}

			r = colorStrength[r];
			g = colorStrength[g];
			b = colorStrength[b];
		}
	}

	uint8_t start;
	uint8_t end;
	int splitIndex = ledPart.indexOf('-');
	if (splitIndex != -1) {
		String startString = ledPart.substring(0, splitIndex);
		String endString = ledPart.substring(splitIndex+1);
		start = parseVarnumber(startString, F("<start>"), command);
		end = parseVarnumber(endString, F("<end>"), command);
	}
	else {
		if (ledPart == F("a")) {
			start = 0;
			end = NUM_LEDS - 1;
		}
		else {
			start = parseVarnumber(ledPart, F("(led part <single led>)"), command);
			end = start;
		}
	}
	

	if (!errorState) {
		debug(F("\tCOLOR-OK: "));
		debug(r);
		debug(F(":"));
		debug(g);
		debug(F(":"));
		debug(b);
		debug(F("  "));
		debug(start);
		debug(F(" - "));
		debugln(end);

		CRGB color = CRGB(r, g, b);
		for (int i = start; i <= end; i++) {
			leds[i] = color;
		}
	}
	else {
		debugln(F("COLOR-ERROR"));
	}

	return errorState;
}

boolean parseDelay(String argument, String command) {
	if (argument.length() < 2) {
		return parseError(F("command delay too short"), command, command, command);
	}

	uint8_t timeUnitIndex = argument.length() - 1;
	char timeUnit = argument.charAt(timeUnitIndex);
	long multiplication = 0;
	switch(timeUnit) {
		case 's' : multiplication = 1000; break;
		case 'd' : multiplication = 100; break;
		case 'c' : multiplication = 10; break;
		case 'm' : multiplication = 1; break;
		default:
			return parseError(F("Unknown time-unit type"), String(timeUnit), F("<time-unit>"), command);
	}
	String value = argument.substring(0, timeUnitIndex);
	uint8_t delayValue = parseVarnumber(value, F("<value>"), command);

	long newdelayMillis = multiplication * delayValue;

	debug(F("     DELAY-OK : "));
	debug(delayValue);
	debug(timeUnit);
	debug(F(" = "));
	debug(newdelayMillis);
	debugln(F("ms."));
	delayMillis = newdelayMillis;
	return errorState;
}

boolean parseVariable(String argument, String command) {
	if (argument.length() < 3) {
		return parseError(F("command variable too short"), argument, F("<variable name>(operation)<value>"), command);
	}

	char varName = argument.charAt(0);
	char operation = argument.charAt(1);
	String valueString = argument.substring(2);
	uint8_t value = parseVarnumber(valueString, F("variable argument"), command);

	uint8_t varIndex = varName - 'a';

	switch(operation) {
		case '=' :
			variable[varIndex] = value;
			debug(varName);
			debug(F("="));
			debugln(value);
			break;
 		case '+' :
			variable[varIndex] += value;
			debug(varName);
			debug(F("+="));
			debug(value);
			debug(F("   | new value:"));
			debugln(variable[varIndex]);
			break;
		case '-' :
			variable[varIndex] -= value;
			debug(varName);
			debug(F("-="));
			debug(value);
			debug(F("   | new value:"));
			debugln(variable[varIndex]);
			break;
		default:
			return parseError(F("Unknown variable operation type"), String(operation), F("operation"), command);
	}

	return errorState;
}

boolean parseLabel(String argument, String command) {
	if (!validLabel(argument, F("<label name>"), command)) {
		return errorState;
	}
	debug(F("\t\tlabel: "));
	debugln(argument);

	return errorState;
}


boolean parseGoto(String argument, String command) {
	if (!validLabel(argument, F("<label name>"), command)) {
		return errorState;
	}
	return doGoto(argument, command);
}

boolean doGoto(String label, String command) {
	int nextCommandIndex = findIndexAfterLabel(label);
	if (!communicating && nextCommandIndex == -1) {
		return parseError(F("label not found."), label, F("<label-name>"), command);
	}

	debug(F("\t\tgoto: "));
	debug(label);
	debug(F("\tindex: "));
	debugln(nextCommandIndex);

	commandIndex = nextCommandIndex;

	return errorState;
}

boolean parseCompare(String argument, String command) {
	int pipeIndex = argument.indexOf('|');
	if (pipeIndex == -1) {
		return parseError(F("| missing."), argument, F("<argument>"), command);
	}
	String comparisonPart = argument.substring(0, pipeIndex);
	String label = argument.substring(pipeIndex +1);

	if (!validLabel(label, F("<label name>"), command)) {
		return errorState;
	}
	if (comparisonPart.length() < 3) {
		return parseError(F("Compare command too short"), comparisonPart, F("(comparison part)"), command);
	}

	char varName = comparisonPart.charAt(0);
	char compareChar1 = comparisonPart.charAt(1);
	char compareChar2 = comparisonPart.charAt(2);
	String compareToString;

	uint8_t varValue = getVarValue(varName, F("<variable name"), command );
	if (errorState) { return errorState; }

	boolean compareResult;
	uint8_t compareTo;

	debug(F("Comparing: '"));
	debug(varValue);

	if (compareChar2 == '=') {
		compareToString = comparisonPart.substring(3);
		compareTo = parseVarnumber(compareToString, F("<Compare to>"), command);
		debug(F("="));

		switch (compareChar1) {
			case '=' : compareResult = (varValue == compareTo); break; // ==
			case '<' : compareResult = (varValue <= compareTo); break; // ==
			case '>' : compareResult = (varValue >= compareTo); break; // ==
			default: return parseError(F("Command compare"), comparisonPart, F("<operation>"), command);
		}
	}
	else {
		compareToString = comparisonPart.substring(2);
		compareTo = parseVarnumber(compareToString, F("<Compare to>"), command);
		switch (compareChar1) {
			case '<' : compareResult = (varValue < compareTo); break; // ==
			case '>' : compareResult = (varValue > compareTo); break; // ==
			default: return parseError(F("Command compare"), comparisonPart, F("<operation>"), command);
		}
	}

	debug(compareChar1);
	debug(compareTo);
	debug(F("  -> "));


	if (!errorState && compareResult) {
		debug(F("true"));
		doGoto(label, command);
	}
	else {
		debugln(F("false"));
	}
	return errorState;
}


// -------------------- command helper methods --------------------



uint8_t parseVarnumber(String input, String part, String command) {
	if (input.length() == 0) {
		parseError(F("varnumber length 0"), input, part, command);
		return 0;
	}
	if (input.length() == 1) {
		char possibleVarName = input.charAt(0);
		if (possibleVarName >= 'a' && possibleVarName <= 'z') {
			return getVarValue(possibleVarName, part, command);
		}
	}
	int value = input.toInt();
	if (value < 0 || value > 255) {
		parseError(F("varnumber value not uint_8"), input, part, command);
		return 0;
	}
	return value;
}

int parseVarnumberhex(String input, String part, String command) {
	if (input.length() != 2) {
		parseError(F("varnumberhex does not have length 2"), input, part, command);
		return 0;
	}
	char hexArray[3];
	input.toCharArray(hexArray, 3);

	if (hexArray[0] == ' ') {
		return getVarValue(hexArray[1], part, command);
	}
	else {
		uint8_t delay = (uint8_t) strtol(hexArray, NULL, 16);
		return delay;
	}
}

uint8_t getVarValue(char varName, String part, String command) {
	if (varName < 'a' || varName > 'z') {
		String variableName = String(varName);
		parseError(F("Illegal variable name"), variableName, part, command);
		return 0;
	}
	uint8_t varIndex = varName - 'a';
	return variable[varIndex];
}

boolean parseError(String errorMessage, String input, String part, String command) {
	boolean previousDebugEnabled = debugEnabled;
	debugEnabled = true;

	debug(F("ERROR: "));
	debug(errorMessage);
	debug(F(" "));
	debug(part);
	debug(F("='"));
	debug(input);
	debug(F("' for command: '"));
	debug(command);
	debugln(F("'."));
	errorState = true;

	debugEnabled = previousDebugEnabled;
	return false;
}

boolean validLabel(String label, String part, String command) {
	if (label.length() < 1) {
		parseError(F("label name is too short"), "", part, command);
		return false;
	}
	for (int i = 0; i < label.length(); i++) {
		char c = label.charAt(i);
		if (!validStringChar(c)) {
			parseError(F("label contains invalid char"), String(c), F("<name>"), command);
			return false;
		}
	}
	return true;
}


void clearCommands() {
	debugln(F(" ++ clearCommands"));

	while (commands.size() > 0) {
		debug(F("deleting node data: "));
		String *data = commands.pop();
		debugln(*data);
		delete(data);
	}

	debugln(F(" ++ finished clearCommands"));
}


void resetVars() {
	for(int i = 0; i < 26; i++) {
		variable[i] = 0;
	}
}

int findIndexAfterLabel(String name) {
	for(int i = 0; i < commands.size(); i++) {
		String* command = commands.get(i);
		char type = command->charAt(0);
		if (type != 'l') { continue; }
		String label = command->substring(1);

		if (label == name) {
			int labelIndex = i;
			// if the label is the last command of the script, the next command is the first command.
			if (labelIndex == commands.size() - 1) {
				return 0;
			}
			return labelIndex + 1;
		}
	}
	return -1;
}

void debug(String message) {
	if (debugEnabled){
		Serial.print(message);
	}
}

void debug(long message) {
	if (debugEnabled){
		Serial.print(message);
	}
}

void debug(uint8_t message) {
	if (debugEnabled){
		Serial.print(message);
	}
}

void debug(int message) {
	if (debugEnabled){
		Serial.print(message);
	}
}


void debug(char message) {
	if (debugEnabled){
		Serial.print(message);
	}
}

void debugln(String message) {
	if (debugEnabled){
		Serial.println(message);
	}
}

void debugln(int message) {
	if (debugEnabled){
		Serial.println(message);
	}
}
void debugln(long message) {
	if (debugEnabled){
		Serial.println(message);
	}
}

void debugln(char message) {
	if (debugEnabled){
		Serial.println(message);
	}
}
#include "MemoryFree.h"
#include "LinkedList.h"

struct variable {
	char name;
	uint8_t value;
};

//struct variable *vpointer;
LinkedList<variable*> variables = LinkedList<variable*>();

void setup ()
{
	addVariable('a', 4);
	addVariable('b', 5);
	addVariable('c', 6);
	Serial.begin (115200);
	Serial.println ();
	
	Serial.print("freeMemory()=");
	Serial.println(freeMemory());
}  // end of setup

void addVariable(char name, uint8_t val) {
	struct variable *vpointer = malloc(sizeof(variable));
	vpointer->name = name;
	vpointer->value = val;

	variables.add(vpointer);

}

void loop () {
	for (int i = 0; i < variables.size(); i++) {
		struct variable *vpointer = variables.get(i);
		Serial.print(vpointer->name);
		Serial.print(F("="));
		Serial.println(vpointer->value);
	}

	Serial.print(F("freeMemory()="));
	Serial.println(freeMemory());

	if (variables.size() > 0) {
		Serial.print(F("Removing: "));
		variable *vpointer = variables.pop();
		Serial.println(vpointer->name);
		delete vpointer;
	}
	

	createString1(F("jaap"));
	createString1(F("jaap"));

	//Serial.println(* res);
	
	delay(2000);
	
	
}

void createString1(String input) {
	// 1830
	Serial.println(12); // 1826
	String temp1 = F("123"); // 1826
	String temp2 = F("abc"); // 1826
	//String temp3 = F("123"); // 1826

}	
#include <FastLED.h>
#include <Math.h>
#define NUM_LEDS 28
#define DATA_PIN 2
#define CLOCK_PIN 3

#define SENSOR_MIN 500
#define SENSOR_MAX 800

int sensorPin = A7; // select the input pin for LDR
int sensorValue = 0; // variable to store the value coming from the sensor
CRGB leds[NUM_LEDS];
uint8_t max_bright = 8;


void setup() {
  Serial.begin(9600); //sets serial port for communication
    FastLED.addLeds<APA102, DATA_PIN, CLOCK_PIN, BGR>(leds, NUM_LEDS);
  FastLED.setBrightness(max_bright);
}

void loop() {
  sensorValue = analogRead(sensorPin); // read the value from the sensor


  //  880 = light
  // 570 = covered
  // 400 = smothered

  int sensorCapped = min(max(SENSOR_MIN, sensorValue), SENSOR_MAX);

  int sensorZeroBased = sensorCapped - SENSOR_MIN;

  float sensorLog = log10(sensorZeroBased) * 10;

  int ledsToLight = map(sensorLog, 0,27, 28,0);
  String message = "S: ";
  message =+ sensorValue;
  message += ", leds: ";
  message += ledsToLight;
//  message += ", normalized: ";
//  message += normalized;

  Serial.println(message); //prints the values coming from the sensor on the screen


//  int i = 0;
//  for (i = 0; i < ledsToLight; i++ ) {
//    leds[i] = CRGB(i * 9, 128 - i* 4, 0);
//  }
//  for (i = ledsToLight; i < NUM_LEDS; i++) {
//    leds[i] = CRGB::Black;
//  }
  int i = 0;
  int j = 0;
  while (true) {
    leds[i] = CRGB(0,0,0);
    i = (i+1) % NUM_LEDS;
    j = (j+1) % 27;
    leds[i] = CRGB(j * 9, 128 - j* 4, 0);
    
     

//    leds[5] = CRGB(255,255,255);
    FastLED.show();       
    delay(100);  
  }
}

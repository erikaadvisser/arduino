#define LED_RED 2
#define LED_ORANGE 3
#define LED_YELLOW 4
#define LED_BLUE 6

#define RCWL_0516_DETECT  8


void setup() {
	pinMode(LED_RED, OUTPUT);
	pinMode(LED_ORANGE, OUTPUT);
	pinMode(LED_YELLOW, OUTPUT);
	pinMode(LED_BLUE, OUTPUT);
	pinMode(RCWL_0516_DETECT, INPUT);

	blink(LED_YELLOW);
	blink(LED_ORANGE);
	blink(LED_RED);
	blink(LED_BLUE);
	//digitalWrite(LED_YELLOW, HIGH);
	//delay(300);
	//digitalWrite(LED_YELLOW, LOW);
	//digitalWrite(LED_ORANGE, HIGH);
	//delay(300);
	//digitalWrite(LED_ORANGE, LOW);
	//digitalWrite(LED_RED, HIGH);
	//delay(300);
	//digitalWrite(LED_RED, LOW);
	//digitalWrite(LED_BLUE, HIGH);
	//delay(100);
	//digitalWrite(LED_BLUE, LOW);
	//delay(100);
	//digitalWrite(LED_BLUE, HIGH);
	//delay(100);
	//digitalWrite(LED_BLUE, LOW);
	//delay(100);
}

void blink(int pin) {
	digitalWrite(pin, HIGH);
	delay(300);
	digitalWrite(pin, LOW);
	delay(200);
	digitalWrite(pin, HIGH);
	delay(200);
	digitalWrite(pin, LOW);
	delay(150);
	digitalWrite(pin, HIGH);
	delay(100);
	digitalWrite(pin, LOW);
	delay(50);
}

// 0 = ok. 1 = YELLOW, 2 = ORANGE, 3 = RED FLASHING
int state = 0;
boolean flash_state = false;

int previousDetect = LOW;

void loop() {
	int detect = digitalRead(RCWL_0516_DETECT);

	if (detect == HIGH) {
		if (previousDetect == LOW) {
			digitalWrite(LED_BLUE, HIGH);
			processStateToHigh();
		}
	}
	else {
		if (previousDetect == HIGH) {
			digitalWrite(LED_BLUE, LOW);
			processStateToLow();
		}
	}
	
	processCurrentState();
	
	previousDetect = detect;
	delay(100);
}

void processStateToHigh() {
	if (state == 0) {
		state = 1;
		digitalWrite(LED_YELLOW, HIGH);
		return;
	}
	if (state == 1) {
		state = 2;
		digitalWrite(LED_YELLOW, LOW);
		digitalWrite(LED_ORANGE, HIGH);
		return;
	}
	if (state == 2) {
		state = 3;
		digitalWrite(LED_ORANGE, LOW);
		flash_state = true;
		return;
	}
}

void processStateToLow() {
	return;
}

void processCurrentState() {
	if (state != 3) {
		return;
	}
	if (flash_state) {
		digitalWrite(LED_RED, HIGH);
	}
	else {
		digitalWrite(LED_RED, LOW);
	}
	flash_state = !flash_state;
}

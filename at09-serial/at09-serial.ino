#include <Adafruit_GFX.h>
#include <SoftwareSerial.h>
#include <Math.h>
#include <elapsedMillis.h>
#include "MemoryFree.h"
#include "LinkedList.h"

#define LED_1 2


#define AT09_RX 8
#define AT09_TX 9

SoftwareSerial bleSerial(AT09_RX, AT09_TX);
elapsedMillis timeElapsed;
LinkedList<String*> commands = LinkedList<String*>();
boolean errorState = false;

void setup(){
	bleSerial.begin(9600);
	Serial.begin(115200);
	pinMode(LED_1, OUTPUT);


	digitalWrite(LED_1, HIGH);
	delay(75);
	digitalWrite(LED_1, LOW);
	delay(75);
	digitalWrite(LED_1, HIGH);
	delay(75);
	digitalWrite(LED_1, LOW);
	delay(75);
	digitalWrite(LED_1, HIGH);
	delay(500);
	digitalWrite(LED_1, LOW);

	Serial.print(F("freeMemory()="));
	Serial.println(freeMemory());


}

void loop() {
	bleMain();

	delayMicroseconds(100);
}


void bleMain() {
	if (bleSerial.available()) {
		Serial.println(F("--- --- --- new message --- --- ---"));

		boolean ok = readCommunicationHeader();
		if (ok) {
			boolean error = readEffectCommunication();
		}
		discardRemainingCommunication();
	}
}

boolean readCommunicationHeader() {
	timeElapsed = 0;
	String header = "";

	while (true) {
		if (bleSerial.available()) {
			timeElapsed = 0;
			char c = bleSerial.read();
			header.concat(c);
			if (header.length() == 3) {
				break;
			}
		}
		else {
			delayMicroseconds(1);
			if (timeElapsed > 250) {
				return false;
			}
		}
	}

	String LIGHT_EFFECT_HEADER = "*l{";
	return (LIGHT_EFFECT_HEADER == header);
}


boolean readEffectCommunication() {
	clearCommands();
	errorState = false;
	timeElapsed = 0;
	long transmissionStart = millis();

	boolean finished = false;
	String message = "";
	while (!finished) {
		if (bleSerial.available()) {
			timeElapsed = 0;
			char c = bleSerial.read();
			if (c == '}') {
				break;
			}

			if (c == ';') {
				Serial.print(F("Received: '"));
				Serial.print(message);
				Serial.print(F(";'    - "));
				finished = parseMessage(message);
				message = "";
			}
			else {
				message.concat(c);
			}
		}
		else {
			delayMicroseconds(1);
			if (timeElapsed > 250) {
				Serial.println(F("timeout during read."));
				errorState = true;
				break;
			}
		}
	}


	Serial.print(F("freeMemory()="));
	Serial.println(freeMemory());

	long transmissionEnd = millis();

	Serial.println(F(""));
	Serial.print(F("Parsing took: "));
	Serial.print(transmissionEnd - transmissionStart);
	Serial.print(F(" millis.     "));
	if (errorState) {
		Serial.println(F("Error occurred during parsing, effect rejected."));
	}
	else {
		Serial.println(F("Parse success!"));
	}

	return errorState;
}

boolean discardRemainingCommunication() {
	timeElapsed = 0;
	boolean finished = false;

	String toDiscard = "";
	while (!finished) {
		if (bleSerial.available()) {
			timeElapsed = 0;
			char c = bleSerial.read();
			toDiscard.concat(c);
		}
		else {
			delayMicroseconds(1);
			if (timeElapsed > 250) {
				finished = true;
			}
		}
	}

	Serial.print(F("Discarding: '"));
	Serial.print(toDiscard);
	Serial.println(F("'."));
}



boolean parseMessage(String command) {
	if (command.length() == 0) {
		return true;
	}
	char type = command.charAt(0);
	String argument = command.substring(1);

	boolean finished = paresCommand(type, argument, command);

	if (!finished) {
		String *toAdd = new String(command);
		commands.add(toAdd);
	}

	return finished;
}

boolean paresCommand(char type, String argument, String command) {
	switch(type) {
		case 'c' : return parseColor(argument, command);
		case 'd' : return parseDelay(argument, command);
		default  :
					String tString = String(type);
					return parseError("Unknown command type", tString, "type", command);
	}
}

boolean parseColor(String argument, String command) {
	//Serial.println(F("color: '" + argument);

	String rr = argument.substring(0,2);
	String gg = argument.substring(2,4);
	String bb = argument.substring(4,6);

	String ledsPart = argument.substring(7);
	int splitIndex = ledsPart.indexOf('-');
	String startString = ledsPart.substring(0, splitIndex);
	String endString = ledsPart.substring(splitIndex+1);


	Serial.print(F("mem()="));
	Serial.print(freeMemory());

	uint8_t r = parseVarnumberhex(rr, "<rr>", command);
	uint8_t g = parseVarnumberhex(gg, "<gg>", command);
	uint8_t b = parseVarnumberhex(bb, "<bb>", command);
	uint8_t start = parseVarnumber(startString, "<start>", command);
	uint8_t end = parseVarnumber(endString, "<end>", command);

	if (!errorState) {
		Serial.println(F("COLOR-OK"));
	}
	else {
		Serial.println(F("COLOR-ERROR"));
	}

	//Serial.print(F("Color: r:"));
	//Serial.print(r);
	//Serial.print(F(" g:"));
	//Serial.print(g);
	//Serial.print(F(" b:"));
	//Serial.print(b);
	//Serial.print(F(" ... start:"));
	//Serial.print(start);
	//Serial.print(F(" end:"));
	//Serial.println(end);

	return errorState;
}

boolean parseDelay(String argument, String command) {
	uint8_t delay = parseVarnumber(argument, "<millis>", command);

	//Serial.print(F("delay: "));
	//Serial.print(delay);
	//Serial.println(F(" ms."));
	Serial.println(F("     DELAY-OK"));
	return errorState;
}


int parseVarnumber(String input, String part, String command) {
	if (input.length() == 0) {
		parseError("varnumber length 0", input, part, command);
		return 0;
	}
	int delay = input.toInt();
	return delay;
}

int parseVarnumberhex(String input, String part, String command) {
	if (input.length() != 2) {
		parseError("varnumberhex does not have length 2", input, part, command);
		return 0;
	}
	char hexArray[3];
	input.toCharArray(hexArray, 3);
	uint8_t delay = (uint8_t) strtol(hexArray, NULL, 16);
	return delay;
}

boolean parseError(String errorMessage, String input, String part, String command) {
	Serial.print(F("ERROR: "));
	Serial.print(errorMessage);
	Serial.print(F(" "));
	Serial.print(part);
	Serial.print(F("='"));
	Serial.print(input);
	Serial.print(F("' for command: '"));
	Serial.print(command);
	Serial.println(F("'."));
	errorState = true;
	return false;
}

void clearCommands() {
	Serial.println(F(" ++ clearCommands"));

	while (commands.size() > 0) {
		Serial.print(F("deleting node data: "));
		String *data = commands.pop();
		Serial.println(*data);
		delete(data);
	}

	Serial.println(F(" ++ finished clearCommands"));
}
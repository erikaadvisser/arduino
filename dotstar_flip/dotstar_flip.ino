#include <FastLED.h>
#define NUM_LEDS 12
#define DATA_PIN 6
#define CLOCK_PIN 5

//#define NUM_LEDS 60
//#define DATA_PIN 3
//#define CLOCK_PIN 2


CRGB leds[NUM_LEDS];

uint8_t max_bright = 16;

void setup() {
  FastLED.addLeds<APA102, DATA_PIN, CLOCK_PIN, BGR>(leds, NUM_LEDS);
  FastLED.setBrightness(max_bright);
}

void loop() { 
  for (int j = 0; j < NUM_LEDS; j++ ) {
    for (int i = 0; i < NUM_LEDS; i++ ) {
      leds[i] = CRGB::Black;
    }
    leds[j] = CRGB::White;
    FastLED.show();       
    delay(225); 
  }
}
  

#define DATA_PIN 2
#define CLOCK_PIN 3

void setup() {
  pinMode(DATA_PIN, OUTPUT);
  pinMode(CLOCK_PIN, OUTPUT);

  digitalWrite(DATA_PIN, LOW);
  digitalWrite(CLOCK_PIN, LOW);

  pinMode(LED_BUILTIN, OUTPUT);
  for (int j = 1; j <= 3; j += 1) {
    for (int i = 10; i <= 100; i += 10) {
      blink(i);
    }
    delay(200);
  }

}

void blink(unsigned long delayTime) {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(delayTime);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(delayTime);                       // wait for a second
}

void loop() {
  // put your main code here, to run repeatedly:

}
